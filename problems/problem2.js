//Q2 Find all users staying in Germany.

function usersInGermany(data) {
    let result = []
    try {
        for (let keys in data) {
            let nationality = data[keys].nationality
           
                if (nationality.includes("Germany")) {
                    result.push(keys)
                }

            
        }
    } catch (error) {
        console.log(error)
    }
    return result

}

module.exports = usersInGermany