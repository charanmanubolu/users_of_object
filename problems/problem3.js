//Q3 Find all users with masters Degree

function withMaster(data) {
    let result = []
    try {
        for (let keys in data) {
            let masters = data[keys].qualification
            if (masters.includes('Masters')) {
                result.push(keys)
            }

        }
    } catch (error) {
        console.log(error)
    }
    return result
}

module.exports = withMaster